<?php
if (isset($_POST['add']) && isset($_POST['username']) && isset($_POST['password'])) {
    $manager = new UtilisateurManager($con);
    $user = $_POST['username'];
    $pass = $_POST['password'];
    if ($manager->addUser($user, $pass)) {
        header('location: index.php?filename=users&response=success');
    } else {
        header('location: index.php?filename=users&response=failed');
    }

}