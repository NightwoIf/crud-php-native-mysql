<?php
session_start();
if (isset($_POST['auth'])) {
    $manager = new UtilisateurManager($con);
    $user = $_POST["user"] ? $_POST['user'] : null;
    $pass = $_POST["pass"] ? $_POST['pass'] : null;
    if ($user != '' && $pass != '') {
        try {
            if ($manager->getUser($user, $pass) != null) {
                $user = $manager->getUser($user, $pass);
                $_SESSION['session_id'] = $user->getId();
                $_SESSION['session_user'] = $user->getUser();
                $manager->setDate($user->getId());
                header('location: index.php?filename=users');
            } else {
                header('location: .');
                $_SESSION['error'] = 'Nom de utilisateur ou mot de pass incorrect!';
            }
        } catch (PDOException $e) {
            echo "Erreur :" . $e->getMessage();
        }
    }
} else {
    $msg = 'Veuillez remplir les deux champs s\'il vous plait!';
}