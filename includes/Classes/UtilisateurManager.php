<?php


use Models\Utilisateurs;

class UtilisateurManager
{
    private $_db;

    public function __construct(\PDO $db)
    {
        $this->_db = $db;
    }

    public function addUser($user, $pass)
    {
        $prepare = $this->_db->prepare('INSERT INTO utilisateurs (user,password,createdat) VALUES (:login ,:pass, NOW())');
        return $prepare->execute([
            'login' => $user,
            'pass' => $pass
        ]);
    }

    public function getUser($user, $pass)
    {
        $prepared = $this->_db->prepare('SELECT * FROM utilisateurs WHERE user=:login and password=:pass');
        $prepared->bindParam(':login', $user);
        $prepared->bindParam(':pass', $pass);
        $prepared->execute();
        if ($prepared->rowCount() > 0) {
            $row = $prepared->fetch(\PDO::FETCH_ASSOC);
            $user = new Utilisateurs($row['user'], $row['password']);
            $user->setId($row['id']);
            return $user;
        }
        return null;
    }

    public function getUsers($sentpage)
    {
        $page = $sentpage;
        $records_per_page = 5;
        $prepared = $this->_db->prepare('SELECT * FROM utilisateurs ORDER BY id LIMIT :current_page, :record_per_page');
        $prepared->bindValue(':current_page', ($page - 1) * $records_per_page, \PDO::PARAM_INT);
        $prepared->bindValue(':record_per_page', $records_per_page, \PDO::PARAM_INT);
        $prepared->execute();
        $users = $prepared->fetchAll(\PDO::FETCH_ASSOC);
        if (count($users) > 0) {
            return $users;
        }
        return null;
    }

    public function getCount()
    {
        return $this->_db->query('SELECT COUNT(*) FROM utilisateurs')->fetchColumn();
    }

    public function setDate($id)
    {
        $prepared = $this->_db->prepare('UPDATE utilisateurs SET lastlogin = NOW() WHERE id = :id');
        $prepared->bindParam(':id', $id);
        return $prepared->execute();
    }

    public function deleteUser($id)
    {
        $prepared = $this->_db->prepare('DELETE FROM utilisateurs WHERE id = :id');
        return $prepared->execute(['id' => $id]);
    }
    public function updateUser($id,$user,$pass){
        $prepared = $this->_db->prepare('UPDATE utilisateurs SET user=:username, password = :pass WHERE id =:id');
        return $prepared->execute([
            'id' => $id,
            'username' => $user,
            'pass' => $pass
        ]);
    }
}