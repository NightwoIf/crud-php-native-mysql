<?php

namespace Models;
class Utilisateurs
{
    private $_id;
    private $_user;
    private $_password;
    private $_lastlogin;
    private $_createdat;

    public function __construct($user, $password)
    {
        $this->_user = $user;
        $this->_password = $password;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->_id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->_id = $id;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->_user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user)
    {
        $this->_user = $user;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->_password;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password)
    {
        $this->_password = $password;
    }

    /**
     * @return mixed
     */
    public function getLastlogin()
    {
        return $this->_lastlogin;
    }

    /**
     * @param mixed $lastlogin
     */
    public function setLastlogin($lastlogin)
    {
        $this->_lastlogin = $lastlogin;
    }

    /**
     * @return false|string
     */
    public function getCreatedat()
    {
        return $this->_createdat;
    }

    /**
     * @param false|string $createdat
     */
    public function setCreatedat($createdat)
    {
        $this->_createdat = $createdat;
    }


}