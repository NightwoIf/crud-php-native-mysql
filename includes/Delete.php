<?php
session_start();
if (isset($_GET['id'])) {

    if ($_GET['id'] != $_SESSION['session_id']) {
        $manager = new UtilisateurManager($con);
        if ($manager->deleteUser($_GET['id'])) {
            header('location: index.php?filename=users&response=deleted');
        } else {
            header('location: index.php?filename=users&response=notdeleted');
        }
    } else {
        header('location: index.php?filename=users&response=notdeleted');
    }


}