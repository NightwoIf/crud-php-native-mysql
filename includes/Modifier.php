<?php
if(isset($_GET['id']) && isset($_GET['user']) && isset($_GET['pass'])){
    $manager = new UtilisateurManager($con);
    $id = $_GET['id'];
    $user = $_GET['user'];
    $pass = $_GET['pass'];
    if($manager->updateUser($id,$user,$pass)){
        header('location: index.php?filename=users&response=modified');
    }else{
        header('location: index.php?filename=users&response=notmodified');
    }

}