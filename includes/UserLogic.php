<?php

$manager = new UtilisateurManager($con);
$page = isset($_GET['page']) && is_numeric($_GET['page']) ? (int)$_GET['page'] : 1;
$users = $manager->getUsers($page);
$count = $manager->getCount();
$total = 0;
