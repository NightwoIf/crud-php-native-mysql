<?php

//fichier de connection base de données !
$config = (object)array(
    'host' => 'localhost',
    'db' => 'Testdev',
    'user' => 'root',
    'password' => ''
);

try {
    $con = new PDO('mysql:host:=' . $config->host . ';dbname=' . $config->db, $config->user, $config->password);
} catch (PDOException $e) {
    print 'Erreur !: ' . $e->getMessage() . PHP_EOL;
}