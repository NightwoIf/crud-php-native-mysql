<?php
switch (@$_GET['filename']) {
    case 'users':
        require('includes/connection.php');
        require('includes/Classes/Utilisateurs.php');
        require('includes/Classes/UtilisateurManager.php');
        require('includes/UserLogic.php');
        include('templates/user.php');
        break;
    case 'Auth':
        require('includes/connection.php');
        require('includes/Classes/Utilisateurs.php');
        require('includes/Classes/UtilisateurManager.php');
        require('includes/Auth.php');
        break;
    case 'add':
        require('includes/connection.php');
        require('includes/Classes/UtilisateurManager.php');
        require('includes/Add.php');
        break;
    case 'logout':
        header('location: includes/Logout.php');
        break;
    case 'delete':
        require('includes/connection.php');
        require('includes/Classes/UtilisateurManager.php');
        require('includes/Delete.php');
        break;
    case 'save':
        require('includes/connection.php');
        require('includes/Classes/UtilisateurManager.php');
        require('includes/Modifier.php');
        break;
    default:
        if (@$_GET['filename'] == '') {
            include('templates/Login.php');
        } else {
            header('HTTP/1.0 404 Not Found');
            include('templates/page_not_found.php');
        }
        break;
}
