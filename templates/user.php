<?php
session_start();
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Users</title>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="CSS/style.css">
    <?php if (isset($_GET['response'])) { ?>
        <script type="text/javascript">
            $(window).on('load', function () {
                $('#msgmodal').modal('show');
            });
            if (typeof window.history.pushState == 'function') {
                window.history.pushState({}, "Hide", '<?php echo $_SERVER['PHP_SELF'];?>');
            }
        </script>
    <?php } ?>

</head>

<body>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="#">TEST DEV</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup"
            aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
        <ul class="navbar-nav w-100">
            <li class="nav-item dropdown ml-auto">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown"
                   aria-haspopup="true" aria-expanded="false">
                    Bonjour,<?= $_SESSION['session_user']; ?>
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                    <a class="dropdown-item" href="index.php?filename=logout">Logout</a>
                </div>
            </li>
        </ul>
    </div>
</nav>
<div class="container-xl">
    <div class="table-responsive">
        <div class="table-wrapper">
            <div class="table-title">
                <div class="row">
                    <div class="col-sm-6">
                        <h2>Gestion<b> Utilisateurs</b></h2>
                    </div>
                    <div class="col-sm-6">
                        <a href="#addEmployeeModal" class="btn btn-success float-right" data-toggle="modal"><i
                                    class="material-icons">&#xE147;</i> <span>Add New Employee</span></a>
                    </div>
                </div>
            </div>
            <table class="table table-striped table-hover">
                <thead>

                <tr>
                    <th>User</th>
                    <th>Password</th>
                    <th>Created AT</th>
                    <th>Last Login</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($users as $user): ?>
                    <tr>
                        <td><?= $user['user'] ?></td>
                        <td><?= $user['password'] ?></td>
                        <td><?= $user['createdat'] ?></td>
                        <td><?= $user['lastlogin'] ? $user['lastlogin'] : 'Not connected YET'; ?></td>
                        <td>
                            <a href="#editEmployeeModal" class="edit" data-toggle="modal" data-id="<?= $user['id'] ?>"><i class="material-icons"
                                                                                             data-toggle="tooltip"
                                                                                             title="Edit">&#xE254;</i></a>
                            <a href="#deleteEmployeeModal" class="trash" data-id="<?= $user['id'] ?>"
                               data-toggle="modal"><i class="material-icons"
                                                      data-toggle="tooltip"
                                                      title="Delete">&#xE872;</i></a>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
            <div class="clearfix">
                <div class="hint-text">Showing <b><?= $page * 5 < $count ? $total += count($users) : $count; ?></b> out
                    of <b><?= $count; ?></b>
                    entries
                </div>
                <ul class="pagination">
                    <?php if ($page > 1): ?>
                        <li class="page-item disabled"><a href="index.php?filename=users&page=<?= $page - 1; ?>">Previous</a>
                        </li>
                    <?php endif; ?>
                    <?php if ($page * 5 < $count): ?>
                        <li class="page-item"><a href="index.php?filename=users&page=<?= $page + 1; ?>"
                                                 class="page-link">Next</a></li>
                    <?php endif; ?>
                </ul>
            </div>
        </div>
    </div>
</div>
<div id="addEmployeeModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="index.php?filename=add" method="post">
                <div class="modal-header">
                    <h4 class="modal-title">Add User</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>Username</label>
                        <input type="text" name="username" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label>Password</label>
                        <input type="password" name="password" class="form-control" required>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
                    <input type="submit" name="add" class="btn btn-success" value="Add">
                </div>
            </form>
        </div>
    </div>
</div>
<div id="editEmployeeModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="editmodel">Edit User</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="alert alert-danger" id="error" role="alert" style="display: none;">
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>Username</label>
                        <input type="text" id="user"  class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label>Password</label>
                        <input type="password" id="pass" class="form-control" required>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
                    <a href="#" class="btn btn-info" id="modalSave">Save</a>
                </div>
        </div>
    </div>
</div>
<div id="deleteEmployeeModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Delete User</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <div class="modal-body">
                <p>Are you sure you want to delete these Records?</p>
                <p class="text-warning">
                    <small>This action cannot be undone.</small>
                </p>
            </div>
            <div class="modal-footer">
                <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
                <a href="#" class="btn btn-danger" id="modalDelete">Delete</a>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="msgmodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="labelfail">ADD USER</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <?php
                switch (@$_GET['response']){
                    case 'success':
                        echo 'User has been added !';
                        break;
                    case 'failed':
                        echo 'user has not been added...';
                        break;
                    case 'deleted':
                        echo 'user has been deleted!';
                        break;
                    case 'notdeleted':
                        echo "user didn't get deleted...";
                        break;
                    case 'modified':
                        echo "User data has been modified !";
                        break;
                    case 'notmodified':
                        echo "User data failed to modify :c!";
                        break;
                }
                ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<script>
    $('.trash').click(function () {
        //get cover id
        var id = $(this).data('id');
        //set href for cancel button
        $('#modalDelete').attr('href', 'index.php?filename=delete&id=' + id);
    });
    $('#modalSave').click(function () {
        if(!$('#user').val() || !$('#pass').val()){
            $('#error').text('veuillez remplir tous les champs svp!');
            $('#error').css("display","block");
        }else {
            let id = $('.edit').data('id');
            let user = $('#user').val();
            let pass = $('#pass').val();
            $('#modalSave').attr('href', 'index.php?filename=save&id=' + id+'&user='+user+'&pass='+pass);
        }
    })
</script>
</body>
</html>